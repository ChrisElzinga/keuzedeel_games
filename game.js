function ShowNext() {

    if (parseInt(GetAnswer().toString()) === parseInt(Solution.value.toString())) {
        var TotalRight = document.getElementById("TotalRight");
        var Count = TotalRight.innerHTML;
        Count++;
        TotalRight.innerHTML = Count;
        console.log(Count);
        document.getElementById("ConfirmAnswer").disabled = true;
    } else {
        var TotalWrong = document.getElementById("TotalWrong");
        var UnCount = TotalWrong.innerHTML;
        UnCount++;
        TotalWrong.innerHTML = UnCount;
    }
}

function nextButtonRefresh() {
    $("#Binary1 input").val(RandomNumber(1));
    $("#Binary2 input").val(RandomNumber(1));
    $("#Binary3 input").val(RandomNumber(1));
    $("#Binary4 input").val(RandomNumber(1));
    $("#Binary5 input").val(RandomNumber(1));
    $("#Binary6 input").val(RandomNumber(1));
    $("#Binary7 input").val(RandomNumber(1));

    document.getElementById("ConfirmAnswer").disabled = false;
}

function ShowExplanation() {
      document.getElementById("BinaryExplanation").style.display = "block";
    }
    
function ShowExplanationNone() {
      document.getElementById("BinaryExplanation").style.display = "none";
    }

function RandomNumber(len) {
      var RandomNumber;
      var n = '';

      for(var count = 0; count < len; count++) {
        RandomNumber = Math.floor(Math.random() * 2);
        n += RandomNumber.toString();
      }
      return n;
}

    function initiateTimer() {
        this.TIMER = new TIMER("#timer");
        this.TIMER.start();
    }

/**
 * @return {number}
 */
function GetAnswer() {

    let answer = (parseInt($("#Binary1 input").val()) * 64) + (parseInt($("#Binary2 input").val()) * 32) + (parseInt($("#Binary3 input").val()) * 16) +
        (parseInt($("#Binary4 input").val()) * 8) + (parseInt($("#Binary5 input").val()) * 4) + (parseInt($("#Binary6 input").val()) * 2) + (parseInt($("#Binary7 input").val()))

    return answer;
}

$("#Binary1 input").val(RandomNumber(1));
$("#Binary2 input").val(RandomNumber(1));
$("#Binary3 input").val(RandomNumber(1));
$("#Binary4 input").val(RandomNumber(1));
$("#Binary5 input").val(RandomNumber(1));
$("#Binary6 input").val(RandomNumber(1));
$("#Binary7 input").val(RandomNumber(1));