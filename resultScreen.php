<?php

session_start();

include_once 'Connection.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>resultScreen</title>

    <!-- bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script type="text/javascript" src="resultScreen.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel ="stylesheet"
          type="text/css"
          href="resultScreen.css" />
</head>
<body>

<div class="row" id="LeaderBoard">
    <h3 class="col-md-4" id="LbName">Name: <br>
        <?php
        $sql = "SELECT Name FROM leaderboard ORDER BY `leaderboard`.`Score` DESC LIMIT 20";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo $row["Name"]. "<br>";
            }
        } else {
            echo "0 results";
        }
        ?>
    </h3>

    <h3 class="col-md-4" id="LbScore">Score: <br>
        <?php
        $sql = "SELECT Score FROM leaderboard ORDER BY `leaderboard`.`Score` DESC LIMIT 20";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo $row["Score"]. "<br>";
            }
        } else {
            echo "0 results";
        }
        ?>
    </h3>

    <h3 class="col-md-4" id="LbDate"> Date: <br>
        <?php
        $sql = "SELECT Date FROM leaderboard ORDER BY `leaderboard`.`Score` DESC LIMIT 20";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo $row["Date"]. "<br>";
            }
        } else {
            echo "0 results";
        }
        ?>
    </h3>
</div>

<div class="row" id="LeaderBoardScores">

</div>


</body>
</html>
