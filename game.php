<?php

 session_start();

?>

<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="game.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body onload="initiateTimer()">
<div class="row" >
    <div id="uitleg" class="col">
        <button onclick="ShowExplanation()" class="btn btn-warning">Uitleg</button>
    </div>

    <form action="Send.php" method="get">
    <div id="RightWrong" style=" margin-right: 50px; text-align: center;  font-size: 1.5em">
        <div  id="RightWrongAnswer" class="col">
            <div style="display: inline-block;" id="TotalRight" >0</div> <i id="RightAnswer" class="fa fa-check"></i>
            <div style="display: inline-block;" id="ScoreBreak" >  </div>
            <div  style="display: inline-block;" id="TotalWrong">0</div> <i id="WrongAnswer" class="fa fa-times"></i>
        </div>
    </div>
    </form>
</div>

<form action="Send.php" method="get">
<div id="NickName" class="col-md-12 text-center">
    Welcome <?php echo $_GET["NickName"]; ?>
</div>
</form>

<div class="timer" id="timer" style=" text-align: center; font-size: 3em">
    <div style=" display: inline-block" id="minutes" >00</div>
    <div style=" display: inline-block" >:</div>
    <div style=" display: inline-block" id="seconds">00</div>
</div>

<div id="BinaryTips" class="row">
    <div id="BinaryTips1" class="col">
        64
    </div>
    <div id="BinaryTips2" class="col">
        32
    </div>
    <div id="BinaryTips3" class="col">
        16
    </div>
    <div id="BinaryTips4" class="col">
        8
    </div>
    <div id="BinaryTips5" class="col">
        4
    </div>
    <div id="BinaryTips6" class="col">
        2
    </div>
    <div id="BinaryTips7" class="col">
        1
    </div>
</div>

<div id="BinaryQuestion" class="row">
    <div id="Binary1" class="col">
        <input type="text" class="form-control" aria-describedby="basic-addon2" disabled>
    </div>
    <div id="Binary2" class="col">
        <input type="text" class="form-control" aria-describedby="basic-addon2" disabled>
    </div>
    <div id="Binary3" class="col">
        <input type="text" class="form-control" aria-describedby="basic-addon2" disabled>
    </div>
    <div id="Binary4" class="col">
        <input type="text" class="form-control" aria-describedby="basic-addon2" disabled>
    </div>
    <div id="Binary5" class="col">
        <input type="text" class="form-control" aria-describedby="basic-addon2" disabled>
    </div>
    <div id="Binary6" class="col">
        <input type="text" class="form-control" aria-describedby="basic-addon2" disabled>
    </div>
    <div id="Binary7" class="col">
        <input type="text" class="form-control" aria-describedby="basic-addon2" disabled>
    </div>
</div>


<div id="AnswerField" class="input-group col-md-6">
    <input type="text" id="Solution" class="form-control" placeholder="Antwoord" aria-describedby="basic-addon2">
    <div class="input-group-append">
        <button onclick="ShowNext(); nextButtonRefresh(); document.getElementById('Solution').value = ''" class="btn btn-success" id="ConfirmAnswer">confirm</button>
    </div>
</div>

<div style="display:none;" id="NextButton">
    <button type="submit" class="btn btn-success" onclick="window.location = 'POST.php'; ">Next</button>
</div>

<div id="BinaryExplanation" onclick="ShowExplanationNone()">
    <div id="UitlegText">Binair is een numeriek systeem dat alleen bestaat uit de nummers 0 en 1.
       Er wordt opgeteld in een volgorde van 1-2-4-8-16-32-64 en wordt geteld van rechts naar links.
        Hierom is dus bij 0100111 het antwoord 39.
        Probeer het zelf maar eens uit!
    </div>
</div>
<script type="text/javascript" src="game.js"></script>
<script type="text/javascript">
    var str = document.getElementById("NickName").innerHTML;
    var res = str.split(" Welcome ").join('');
</script>
<script type="text/javascript">
    document.getElementById("NextButton").onclick = function () {
        location.href = "Send.php?Nickname=" + res + "&" + "TotalRight=" + document.getElementById("TotalRight").innerHTML;
    };
</script>
<script type="text/javascript" src="Timer.js"></script>
</body>